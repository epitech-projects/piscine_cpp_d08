//
// droid.hh for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d08/ex00
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Wed Jan 15 10:10:59 2014 Jean Gravier
// Last update Thu Jan 16 00:25:34 2014 Jean Gravier
//

#ifndef DROID_HH_
#define DROID_HH_

#include <string>

class		Droid
{
public:
  Droid(std::string = "");
  Droid(Droid const&);
  ~Droid();

public:
  std::string	getId() const;
  void		setId(std::string);
  size_t	getEnergy() const;
  void		setEnergy(size_t);
  size_t	getAttack() const;
  size_t	getToughness() const;
  std::string	*getStatus() const;
  void		setStatus(std::string *);
  bool		isEqual(Droid const&) const;
  void		reloadEnergy(size_t &) const;
  void		showInformations(std::ostream &) const;
  Droid		&operator=(Droid const&);
  void		operator<<(size_t &);

private:
  std::string	_id;
  size_t	_energy;
  size_t const	_attack;
  size_t const	_toughness;
  std::string	*_status;
};

std::ostream		&operator<<(std::ostream &, Droid const&);
bool			operator==(Droid const&, Droid const&);
bool			operator!=(Droid const&, Droid const&);

#endif /* !DROID_H_ */
