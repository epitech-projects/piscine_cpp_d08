//
// droid.hh for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d08/ex00
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Wed Jan 15 10:10:59 2014 Jean Gravier
// Last update Wed Jan 15 19:18:33 2014 Jean Gravier
//

#ifndef DROID_HH_
#define DROID_HH_

#include <string>
#include "droidmemory.hh"

class		Droid
{
public:
  Droid(std::string);
  Droid(Droid &);
  ~Droid();

public:
  std::string	getId();
  void		setId(std::string);
  size_t	getEnergy();
  void		setEnergy(size_t);
  size_t	getAttack();
  size_t	getToughness();
  std::string	*getStatus();
  DroidMemory	*getBattleData();
  void		setBattleData(DroidMemory *);

public:
  void		setStatus(std::string *);
  bool		isEqual(Droid &) const;
  void		reloadEnergy(size_t &) const;
  void		showInformations(std::ostream &) const;
  Droid		&operator=(Droid &);
  void		operator<<(size_t &);

private:
  std::string	_id;
  size_t	_energy;
  size_t const	_attack;
  size_t const	_toughness;
  std::string	*_status;
  DroidMemory	*_battleData;
};

std::ostream		&operator<<(std::ostream &, Droid &);
bool			operator==(Droid &, Droid &);
bool			operator!=(Droid &, Droid &);

#endif /* !DROID_H_ */
