//
// droidmemory.hh for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d08/ex01
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Wed Jan 15 16:15:23 2014 Jean Gravier
// Last update Wed Jan 15 19:49:32 2014 Jean Gravier
//

#ifndef _DROIDMEMORY_H_
#define _DROIDMEMORY_H_

#include <string>
#include <iostream>

class		DroidMemory
{
public:
  DroidMemory();
  ~DroidMemory();

public:
  size_t	getFingerPrint();
  void		setFingerPrint(size_t);
  size_t	getExp();
  void		setExp(size_t);
  void		showInformations(std::ostream &) const;

public:
  DroidMemory	&operator<<(DroidMemory &);
  DroidMemory	&operator>>(DroidMemory &);
  DroidMemory	&operator+=(size_t const&);
  DroidMemory	&operator+=(DroidMemory &);
  DroidMemory	&operator+(DroidMemory &);

private:
  size_t	_fingerPrint;
  size_t	_exp;
};

std::ostream	&operator<<(std::ostream &, DroidMemory &);

#endif /* _DROIDMEMORY_H_ */
