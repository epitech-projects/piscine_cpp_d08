//
// droidmemory.cpp for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d08/ex01
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Wed Jan 15 16:24:49 2014 Jean Gravier
// Last update Wed Jan 15 20:17:06 2014 Jean Gravier
//

#include "droidmemory.hh"
#include <string>
#include <cstdlib>
#include <fstream>
#include <iostream>

DroidMemory::DroidMemory()
{
  this->_exp = 0;
  this->_fingerPrint = random();
}

DroidMemory::~DroidMemory()
{

}

size_t		DroidMemory::getFingerPrint()
{
  return (this->_fingerPrint);
}

void		DroidMemory::setFingerPrint(size_t fingerPrint)
{
  this->_fingerPrint = fingerPrint;
}

size_t		DroidMemory::getExp()
{
  return (this->_exp);
}

void		DroidMemory::setExp(size_t exp)
{
  this->_exp = exp;
}

void		DroidMemory::showInformations(std::ostream &stream) const
{
  stream << "DroidMemory '" << this->_fingerPrint << "', " << this->_exp;
}

DroidMemory	&DroidMemory::operator<<(DroidMemory &memory)
{
  this->_exp += memory._exp;
  this->_fingerPrint ^= memory.getFingerPrint();
  return (*this);
}

DroidMemory	&DroidMemory::operator>>(DroidMemory &memory)
{
  memory.setExp(memory.getExp() + this->_exp);
  memory.setFingerPrint(memory._fingerPrint ^ this->_fingerPrint);
  return (memory);
}

DroidMemory	&DroidMemory::operator+=(DroidMemory &memory)
{
  return (*this << memory);
}

DroidMemory	&DroidMemory::operator+=(size_t const& exp)
{
  this->_exp += exp;
  this->_fingerPrint ^= exp;
  return (*this);
}

DroidMemory	&DroidMemory::operator+(DroidMemory &memory)
{
  DroidMemory	new_memory;

  return (new_memory << memory);
}

std::ostream	&operator<<(std::ostream &stream, DroidMemory &memory)
{
  memory.showInformations(stream);
  return (stream);
}
