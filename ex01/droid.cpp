//
// droid.cpp for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d08/ex00
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Wed Jan 15 10:14:22 2014 Jean Gravier
// Last update Wed Jan 15 19:20:07 2014 Jean Gravier
//

#include <iostream>
#include <fstream>
#include <string>
#include "droid.hh"

/*********************
 ********DROID********
 ********************/

Droid::Droid(std::string Id)
  : _id(Id), _energy(50), _attack(25), _toughness(15), _status(new std::string("Standing by"))
{
  std::cout << "Droid '" << this->_id << "' Activated" << std::endl;
}

Droid::Droid(Droid & droid)
  : _id(droid._id), _energy(droid._energy), _attack(droid._attack), _toughness(droid._toughness), _status(new std::string(droid._status->c_str()))
{
  std::cout << "Droid '" << this->_id << "' Activated, Memory Dumped" << std::endl;
}

Droid::~Droid()
{
  delete this->_status;
  std::cout << "Droid '" << this->_id << "' Destroyed" << std::endl;
}

std::string	Droid::getId()
{
  return (this->_id);
}

void		Droid::setId(std::string id)
{
  this->_id = id;
}

size_t		Droid::getEnergy()
{
  return (this->_energy);
}

void		Droid::setEnergy(size_t energy)
{
  this->_energy = energy;
}

size_t		Droid::getAttack()
{
  return (this->_attack);
}

size_t		Droid::getToughness()
{
  return (this->_toughness);
}

std::string	*Droid::getStatus()
{
  return (this->_status);
}

DroidMemory	*Droid::getBattleData()
{
  return (this->_battleData);
}

void		Droid::setBattleData(DroidMemory *battleData)
{
  this->_battleData = battleData;
}

void		Droid::setStatus(std::string *status)
{
  this->_status = status;
}

bool		Droid::isEqual(Droid & droid) const
{
  if (this->_status == droid._status)
    return (true);
  return (false);
}

void	Droid::showInformations(std::ostream &flux) const
{
  flux << "Droid '" << this->_id << "', " << this->_status->c_str() << ", " << this->_energy;
}

Droid		&Droid::operator=(Droid &droid)
{
  if (this != &droid)
    {
      this->_id = droid._id;
      this->_energy = droid._energy;
      this->_status = droid._status;
    }
  return (*this);
}

bool	operator==(Droid &first, Droid &second)
{
  return (first.isEqual(second));
}

bool	operator!=(Droid &first, Droid &second)
{
  return (!first.isEqual(second));
}

void		Droid::operator<<(size_t &energy)
{
  if ((this->_energy + energy) <= 100 && (this->_energy + energy) == 0)
    this->setEnergy(energy);
  else if ((this->_energy + energy) > 100)
    {
      energy -= (100 - this->_energy);
      this->_energy = 100;
    }

}

std::ostream		&operator<<(std::ostream &flux, Droid &droid)
{
  droid.showInformations(flux);
  return (flux);
}
